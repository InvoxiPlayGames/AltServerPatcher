﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AltServerPatcher
{
    public partial class PatcherWindow : Form
    {
        private static string EXEPath = "";
        private static bool IsAdministrator => new WindowsPrincipal(WindowsIdentity.GetCurrent()).IsInRole(WindowsBuiltInRole.Administrator);

        private static byte[] EXEBytes;

        private static int URLStart = 0;

        public PatcherWindow()
        {
            InitializeComponent();
        }

        private byte[] CreateSpacedString(string ToSpace)
        {
            byte[] first = Encoding.ASCII.GetBytes(ToSpace);
            List<byte> output = new List<byte>();
            for (int i = 0; i < first.Length; i++)
            {
                output.Add(first[i]);
                output.Add(0);
            }

            return output.ToArray();
        }

        private string CreateNormalString(string ToNormal)
        {
            byte[] first = Encoding.ASCII.GetBytes(ToNormal);
            List<byte> output = new List<byte>();
            for (int i = 0; i < first.Length; i+=2)
            {
                output.Add(first[i]);
            }

            return Encoding.ASCII.GetString(output.ToArray());
        }

        private string PadTo55(string ToPad)
        {
            if (ToPad.Length > 55)
            {
                return "https://f000.backblazeb2.com/file/altstore/altstore.ipa";
            } else if (ToPad.Length == 55)
            {
                return ToPad;
            } else
            {
                string output;
                if (ToPad.Contains("?"))
                {
                    output = ToPad + "&";
                } else
                {
                    output = ToPad + "?";
                }
                while (output.Length < 55)
                {
                    output += "a";
                }
                return output;
            }
        }

        private void PatcherWindow_Load(object sender, EventArgs e)
        {
            if (!IsAdministrator)
            {
                MessageBox.Show("Please run AltServer Patcher as an administrator account.", "AltServer Patcher", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Close();
                Application.Exit();
                return;
            }

            if (File.Exists("C:\\Program Files (x86)\\AltServer\\AltServer.exe"))
            {
                EXEPath = "C:\\Program Files (x86)\\AltServer\\AltServer.exe";
            } else if (File.Exists("C:\\Program Files\\AltServer\\AltServer.exe"))
            {
                EXEPath = "C:\\Program Files\\AltServer\\AltServer.exe";
            } else
            {
                string input = Interaction.InputBox("Please enter the exact path to AltServer.exe on your system.", "AltServer Patcher");
                if (File.Exists(input))
                {
                    EXEPath = input;
                } else
                {
                    MessageBox.Show("That file does not exist. Quitting.", "AltServer Patcher", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Close();
                    Application.Exit();
                    return;
                }
            }

            if (!File.Exists(EXEPath + ".bak"))
            {
                File.Copy(EXEPath, EXEPath + ".bak");
            }

            EXEBytes = File.ReadAllBytes(EXEPath);
            byte[] HTTPBytes = CreateSpacedString("http");
            URLStart = EXEBytes.Locate(HTTPBytes)[0];

            List<byte> foundURL = new List<byte>();
            for (int i = URLStart; i < URLStart+110; i ++)
            {
                foundURL.Add(EXEBytes[i]);
            }
            string currentURL = CreateNormalString(Encoding.ASCII.GetString(foundURL.ToArray()));
            
            if (currentURL == "https://f000.backblazeb2.com/file/altstore/altstore.ipa")
            {
                IPASelector.SelectedIndex = 0;
            } else if (currentURL.StartsWith("https://ipg.pw/altstore/?ipa="))
            {
                string prepad = currentURL.Split('&')[0];
                string name = prepad.Replace("https://ipg.pw/altstore/?ipa=", "");
                IPASelector.SelectedItem = name;
            } else
            {
                IPASelector.SelectedIndex = IPASelector.Items.Count - 1;
                IPAURLBox.Text = currentURL;
            }
        }

        private void IPASelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IPASelector.SelectedIndex == IPASelector.Items.Count-1)
            {
                IPAURLBox.ReadOnly = false;
                IPAURLBox.Text = "https://";
            } else
            {
                IPAURLBox.ReadOnly = true;
                if (IPASelector.SelectedIndex == 0)
                {
                    IPAURLBox.Text = "https://f000.backblazeb2.com/file/altstore/altstore.ipa";
                } else
                {
                    IPAURLBox.Text = "https://ipg.pw/altstore/?ipa=" + IPASelector.SelectedItem;
                }
            }
        }

        private void PatchButton_Click(object sender, EventArgs e)
        {
            Thread patchThread = new Thread(GoPatch);
            patchThread.Start();
        }

        private void GoPatch()
        {
            SetProgress(ProgressBarStyle.Marquee, 0, 100, 0);
            SetStatus("Killing AltServer...");
            Process.Start("taskkill", "/f /im AltServer.exe");
            Thread.Sleep(1000);

            SetStatus("Patching...");
            SetEnabled(false);

            string url = IPAURLBox.Text;
            if (!url.StartsWith("https://") && !url.StartsWith("http://"))
            {
                SetProgress(ProgressBarStyle.Continuous, 0, 100, 0);
                SetStatus("You must enter a valid URL!");
                SetEnabled(true);
                return;
            }
            if (url.Length > 55)
            {
                SetProgress(ProgressBarStyle.Continuous, 0, 100, 0);
                SetStatus("Your URL must be less than 55 characters.");
                SetEnabled(true);
                return;
            }

            SetStatus("Padding out the URL...");
            string paddedURL = PadTo55(url);

            SetStatus("Creating the URL byte array...");
            byte[] byteURL = CreateSpacedString(paddedURL);

            SetStatus("Replacing the bytes...");
            SetProgress(ProgressBarStyle.Continuous, 0, 110, 0);
            int x = 0;
            for (int i = URLStart; i < URLStart + 110; i++)
            {
                SetProgress(ProgressBarStyle.Continuous, x, 110, 0);
                EXEBytes[i] = byteURL[x];
                x++;
            }

            SetStatus("Writing to the file...");
            SetProgress(ProgressBarStyle.Marquee, 0, 100, 0);
            File.WriteAllBytes(EXEPath, EXEBytes);

            SetStatus("Done!");
            SetProgress(ProgressBarStyle.Continuous, 100, 100, 0);
            SetEnabled(true);
            MessageBox.Show("AltServer has been patched to install that IPA file! Make sure you uninstall any other applications installed with AltServer for best results.", "AltServer Patcher", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        delegate void SetStatusCallback(string text);
        private void SetStatus(string text)
        {
            if (this.StatusLabel.InvokeRequired)
            {
                SetStatusCallback d = new SetStatusCallback(SetStatus);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.StatusLabel.Text = text;
            }
        }

        delegate void SetEnabledCallback(bool enabled);
        private void SetEnabled(bool enabled)
        {
            if (this.IPAURLBox.InvokeRequired)
            {
                SetEnabledCallback d = new SetEnabledCallback(SetEnabled);
                this.Invoke(d, new object[] { enabled });
            }
            else
            {
                IPAURLBox.Enabled = enabled;
                IPASelector.Enabled = enabled;
                PatchButton.Enabled = enabled;
            }
        }

        delegate void SetProgressCallback(ProgressBarStyle progress, int count, int max, int min);
        private void SetProgress(ProgressBarStyle progress, int count, int max, int min)
        {
            if (this.StatusProgress.InvokeRequired)
            {
                SetProgressCallback d = new SetProgressCallback(SetProgress);
                Invoke(d, new object[] { progress, count, max, min });
            }
            else
            {
                StatusProgress.Style = progress;
                StatusProgress.Minimum = min;
                StatusProgress.Maximum = max;
                StatusProgress.Value = count;
            }
        }

        private void RestoreOriginalButton_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            SetStatus("Killing AltServer...");
            Process.Start("taskkill", "/f /im AltServer.exe");
            Thread.Sleep(1000);
            File.Copy(EXEPath + ".bak", EXEPath, true);
            MessageBox.Show("AltServer has been restored to its original unpatched state! If this doesn't solve any issues, please reinstall AltServer.", "AltServer Patcher", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
